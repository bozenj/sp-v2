# README #

Spodaj lahko najdete primerček HTML in CSS za vaje pri Spletnem Programiranju.

Seveda pa lahko delate nekaj svojega.
Če ste pozabili:

git clone https://bitbucket.org/asistent/sp-v2

### index.html ###

* Uporabite ga lahko kot osnovo
* Vsebuje nekaj značk
* Če vam ninje skačejo po glavi, nadenite čelado

### style.css ###

* Ima več stila kot Nusret Gökçe
* Popravite ga, da opravite naloge na vajah

# HTML in CSS {#V2}

<div class="vrstica">
  <img src="img/ninja_08_sword_down_right.png">
  <h3><span class="sklop1 oznaka">V2</span></h3>
</div>

Za lažje razumevanje vaj si poglejte vsebino predavanj <span class="sklop1">P3.1</span> [HTML](#html) in <span class="sklop1">P3.2</span> [Prekrivni slogi (CSS)](#prekrivni-slogi-css).

## Uvod - Primer HTML
 
Za osnovo lahko uporabite repozitorij https://bitbucket.org/asistent/sp-v2/. Najbolje, da naredite svoj odcepek (fork),
nato pa verjetno poženete nekaj podobnega:

~~~~ {.bash}
git clone https://{študent}@bitbucket.org/{študent}/sp-v2.git
cd sp-v2
~~~~

V nadaljnjih navodilih se bo smatralo, da delate z index.html iz zgornjega repozitorija.


## Prikaz elementov

* Dodajte na dokument še vsaj eno ninjo.

* Spravite stile v ločeno datoteko.

* Poskrbite, da bo slika uzumaki ostala skrita, naj pa se ista slika pokaže kot ozadje elementa.

* Poskrbite, da bodo div-i uchiha, harune in uzumaki v isti vrsti, kot bi bili kosi teksta. 

* Skrijte div madara, ker ga v teh vajah še ne bomo potrebovali.

* Poskrbite, da se bo kakashi obnašal kot div in bo nad ostalimi elementi znotraj svojega starša.

* S pomočjo flex layouta spravite elemente team7, team8, teamguy in team10 v tabelo, v kateri bo team7 2x večji od ostalih.

## Pozicioniranje elementov

* Poskrbite, da bo kabuto točno 30 pikslov desno in 50 pikslov pod zgornjim levim robom svojega starša.

* Premaknite orochimaru v zgornji desni rob zaslona tako, da tam tudi ostane.

* Premaknite deidara na sredino strani.

## Škatla

* poskrbite, da bo team orochimaru v črni kletki s pikčasto obrobo, debelo vsaj 10 pik

* poskrbite, da bo imel shikamaru senco, debelo vsaj 15 pik.

## Selektorji

* Vsi dobri naj imajo zeleno ozadje

* Vsaka vas naj ima obrobo debeline 5 črk "m"

## SVG

* Narišite smeška (smiley). Bonus, če bo imel 7 repov.

* Spravite ga na zaslon.

* Spremenite barvo z rumene na rdečo.

## Platno (canvas)

* Ustvarite element tipa canvas

* Nanj spravite smeška

* Kaj je hitreje - SVG na strani ali na canvasu?
